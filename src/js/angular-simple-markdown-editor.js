/**
 * AngularJS & Bootstrap simple-markdown-editor plugin v1.0.0
 * (c) 2018 Rodziu <mateusz.rohde@gmail.com>
 * License: MIT
 */
var app = angular.module('simpleMarkdownEditor', []);
/**
 * Editor config
 */
app.provider('markdownEditor', function(){
	// noinspection JSUnusedGlobalSymbols
	this.options = {
		toolbar: [
			['h1', 'h2', 'h3'],
			['b', 'i'],
			['ul', 'ol'],
			['code'],
			['a', 'img'],
			['preview']
		],
		icons: {
			h1: 'fa fa-h1',
			h2: 'fa fa-h2',
			h3: 'fa fa-h3',
			b: 'fa fa-bold',
			i: 'fa fa-italic',
			ul: 'fa fa-list-ul',
			ol: 'fa fa-list-ol',
			code: 'fa fa-code',
			a: 'fa fa-link',
			img: 'fa fa-file-image-o',
			preview: 'fa fa-search'
		},
		lang: {
			h1: 'Nagłówek 1',
			h2: 'Nagłówek 2',
			h3: 'Nagłówek 3',
			b: 'Tekst pogrubiony',
			i: 'Tekst pochylony',
			ul: 'Lista',
			ol: 'Numerowana lista',
			code: 'Kod',
			a: 'Link',
			img: 'Wstaw obrazek',
			preview: 'Podgląd'
		},
		actions: {
			h1: function(text){
				return "# " + text + " #";
			},
			h2: function(text){
				return "## " + text + " ##";
			},
			h3: function(text){
				return "### " + text + " ###";
			},
			b: function(text){
				return "**" + text + "**";
			},
			i: function(text){
				return "*" + text + "*";
			},
			ul: function(text){
				return "* " + text;
			},
			ol: function(text){
				return "1. " + text;
			},
			code: function(text){
				return "```\n" + text + "\n```";
			},
			a: function(text){
				var textPrompt,
					url;
				if(text === '%%%cursor%%%'){
					textPrompt = window.prompt("Anchor");
					if(textPrompt === null){
						return text;
					}
				}
				url = window.prompt("Url");
				if(url !== null){
					return '[' + (textPrompt ? textPrompt : text) + '](' + url + ')';
				}
				return text;
			},
			img: function(text){
				var textPrompt,
					url;
				if(text === '%%%cursor%%%'){
					textPrompt = window.prompt("Alt");
					if(textPrompt === null){
						return text;
					}
				}
				url = window.prompt("Img Url");
				if(url !== null){
					return '![' + (textPrompt ? textPrompt : text) + '](' + url + ')';
				}
				return text;
			}
		},
		addMethod: function(name, icon, lang, callback){
			this.icons[name] = icon;
			this.lang[name] = lang;
			this.actions[name] = callback;
		},
		preview: [function(){
			return function(text){
				if(typeof showdown !== 'undefined'){
					var converter = new showdown.Converter();
					return converter.makeHtml(text);
				}else{
					console.error("No preview method defined! Either attach showdown.js to your project or define custom markdownEditorProvider.preview method in your app.config!");
				}
				return text;
			};
		}]
	};
	this.$get = function(){
		return this.options;
	};
});
/**
 * Editor component
 */
app.component('markdownEditor', {
	templateUrl: 'src/templates/markdown-editor.ng',
	bindings: {
		text: '='
	},
	controllerAs: 'ctrl',
	transclude: true,
	controller: ['$timeout', '$injector', '$q', 'markdownEditor', function($timeout, $injector, $q, markdownEditor){
		var ctrl = this,
			previewFn = $injector.invoke(markdownEditor.preview);
		/**
		 * @type {Array}
		 */
		ctrl.toolbar = markdownEditor.toolbar;
		/**
		 * @type {{}}
		 */
		ctrl.lang = markdownEditor.lang;
		/**
		 * @param buttonName
		 * @returns {string}
		 */
		ctrl.getIcon = function(buttonName){
			return markdownEditor.icons[buttonName];
		};
		/**
		 * @type {{shown: boolean, loading: boolean, data: string}}
		 */
		ctrl.preview = {
			shown: false,
			loading: false,
			data: ''
		};
		/**
		 * @param buttonName
		 */
		ctrl.action = function(buttonName){
			if(buttonName === 'preview'){
				if(ctrl.preview.shown){
					ctrl.preview.shown = false;
				}else{
					ctrl.preview.shown = true;
					ctrl.preview.loading = true;
					$q.when(previewFn(ctrl.text)).then(function(result){
						ctrl.preview.data = result;
						ctrl.preview.loading = false;
					}).catch(function(){
						ctrl.preview.shown = false;
						ctrl.preview.loading = false;
						console.error("Error when loading markdown-editor preview!");
					});
				}
				return;
			}
			var selection = ctrl.getSelection(),
				selectedText = ctrl.text.substring(selection.start, selection.end),
				alteredText = markdownEditor.actions[buttonName](selectedText.length ? selectedText : '%%%cursor%%%'),
				cursor = alteredText.indexOf('%%%cursor%%%'),
				endPosition = 0;
			if(cursor !== -1){
				alteredText = alteredText.replace('%%%cursor%%%', '');
				endPosition = selection.start + cursor;
			}else{
				endPosition = selection.start + alteredText.length;
			}
			ctrl.text = ctrl.text.substring(0, selection.start) + alteredText + ctrl.text.substring(selection.end);
			$timeout(function(){
				ctrl.setSelection(endPosition, endPosition);
				ctrl.focus();
			});
		};
	}]
});
/**
 * Editor wrapper
 */
app.directive('markdownWrap', [function(){
	return {
		restrict: 'A',
		require: '^markdownEditor',
		link: function(scope, element, attr, ctrl){
			ctrl.markdownWrap = element;
		}
	};
}]);
/**
 * Textarea
 */
app.directive('markdownText', [function(){
	return {
		restrict: 'A',
		require: '^markdownEditor',
		link: function(scope, element, attr, ctrl){
			element.on('focus', function(){
				ctrl.markdownWrap.addClass('focus');
			});
			element.on('blur', function(){
				ctrl.markdownWrap.removeClass('focus');
			});
			/**
			 * @returns {{start: number, end: number}}
			 */
			ctrl.getSelection = function(){
				return {
					start: element[0].selectionStart,
					end: element[0].selectionEnd
				}
			};
			/**
			 * @param start
			 * @param stop
			 */
			ctrl.setSelection = function(start, stop){
				element[0].setSelectionRange(start, stop)
			};
			/**
			 */
			ctrl.focus = function(){
				element[0].focus();
			}
		}
	};
}]);
/**
 * Unsafely bind html
 */
app.filter('markdownPreview', ['$sce', function($sce){
	return function(input){
		if(typeof input === 'string'){
			return $sce.trustAsHtml(input);
		}
	};
}]);