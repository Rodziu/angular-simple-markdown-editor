// noinspection JSUnresolvedVariable
module.exports = function(grunt){
	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		copy: {
			main: {
				files: [
					{expand: true, cwd: 'src/css', src: '**', dest: 'dist/css'},
					{expand: true, cwd: 'src/js', src: '*', dest: 'dist/js'}
				]
			}
		},
		ngtemplates: {
			app: {
				src: 'src/templates/**.ng',
				dest: 'dist/js/<%= pkg.name %>.js',
				options: {
					append: true,
					module: 'simpleMarkdownEditor',
					htmlmin: {
						collapseWhitespace: true,
						collapseBooleanAttributes: true,
						removeAttributeQuotes: true,
						removeComments: true,
						removeEmptyAttributes: true,
						removeRedundantAttributes: true,
						removeScriptTypeAttributes: true,
						removeStyleLinkTypeAttributes: true
					}
				}
			}
		},
		uglify: {
			options: {
				banner: '/*! <%= pkg.description %> v.<%= pkg.version %> | (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %> */'
			},
			build: {
				src: 'dist/js/<%= pkg.name %>.js',
				dest: 'dist/js/<%= pkg.name %>.min.js'
			}
		},
		cssmin: {
			target: {
				files: [{
					expand: true,
					cwd: 'dist/css',
					src: ['*.css', '!*.min.css'],
					dest: 'dist/css',
					ext: '.min.css'
				}]
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-angular-templates');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');

	grunt.registerTask('default', ['copy', 'ngtemplates', 'uglify', 'cssmin']);

};