var app = angular.module('exampleApp', ['simpleMarkdownEditor']);
/* Example server-side markdown parsing
app.config(['markdownEditorProvider', function(markdownEditorProvider){
	markdownEditorProvider.preview = ['$q', '$http', function($q, $http){
		return function(text){
			var deferred = $q.defer();
			$http.post('', {parseMarkdown: text}).then(function(response){
				deferred.resolve(response.data);
			}).catch(function(){
				deferred.reject();
			});
			return deferred.promise;
		};
	}];
}]);
*/
//
app.controller('exampleCtrl', [function(){
	var ctrl = this;
	ctrl.text = 'test';
}]);